import numpy as np
import tensorflow as tf

seed = 1337
np.random.seed(seed)
tf.set_random_seed(seed)