import set_random_seed
import os
import numpy as np
import pandas as pd
from keras import optimizers

from seq2seq.seq2seq import SimpleSeq2Seq, Seq2Seq
from create_data import num_samples
from create_data import max_known_len
from create_data import unique_chars
from create_data import label_len


random_mapper = np.random.choice(num_samples, num_samples, replace=False)


def get_batch(batch_size):
    batch_x = np.empty([batch_size, max_known_len, len(unique_chars)])
    batch_y = np.empty([batch_size, label_len, len(unique_chars)])
    sample_dir = os.path.join('data', 'npy', 'samples')
    labels = np.load(os.path.join('data', 'npy', 'labels_ycoord.npy'))

    src_idx, dst_idx = 0, 0
    while True:
        src_idx_mapped = random_mapper[src_idx]
        try:
            batch_x[dst_idx] = np.load(os.path.join(sample_dir, str(src_idx_mapped) + '.npz'))['arr_0']
        except ValueError as e:
            print("\tSkipping src_idx {:d}".format(src_idx_mapped), end='')
            src_idx += 1
            continue

        batch_y[dst_idx] = labels[src_idx_mapped]
        src_idx += 1
        dst_idx += 1

        if src_idx >= num_samples:
            src_idx = 0

        if dst_idx >= batch_size:
            dst_idx = 0
            yield batch_x, batch_y


if __name__ == '__main__':
    batch_size = 512
    nb_epoch = 20

    m = Seq2Seq(input_dim=len(unique_chars), hidden_dim=10, output_length=label_len, output_dim=len(unique_chars),
                depth=(3, 3))
    print(m.summary())

    if 1:
        opt = optimizers.Adagrad(lr=0.01)
    else:
        opt = optimizers.RMSprop(lr=0.001)

    m.compile(loss='mse', optimizer=opt, metrics=['accuracy'])
    m.fit_generator(get_batch(batch_size),
                    steps_per_epoch=int(num_samples / batch_size),
                    nb_epoch=nb_epoch)

