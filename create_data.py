import os
import pandas as pd
import numpy as np

in_file = os.path.join('data', 'parking.csv')
out_dir = 'data'
num_samples = 132850
max_known_len = 393
label_len = 6  # For y_coord
unique_chars = ' *,-./0123456789:>ABCDEFGHIJKLMNOPQRSTUVWXYZ_a'  # Derived by analysis

# -------------------------
create_text = 0
create_label_address = 0
create_label_ycoord = 0
create_npy = 0
# -------------------------

char_map = dict([(char, idx) for idx, char in enumerate(unique_chars)])

if __name__ == '__main__':
    df = pd.read_csv(in_file)

    if create_text:
        out_file = os.path.join(out_dir, 'parking.txt')
        max_len = -1
        f_out = open(out_file, 'w')
        for i in range(len(df)):
            order = np.random.choice(19, 19, replace=False)
            line = df.iloc[i, order].to_string().replace('\n', '')
            line = " ".join(line.split())
            line = line + ' ' * (max_known_len - len(line)) + '\n'
            max_len = max(max_len, len(line) - 1)
            f_out.write(line)

        f_out.close()
        print("Wrote file: {:s} Known_Max_len: {:d} Max_len: {:d}".format(out_file, max_known_len, max_len))

    if create_label_address:
        out_file = 'label_address.csv'
        d = df['ADDRESS_ID'].astype(str)
        d.to_csv(os.path.join(out_dir, out_file), index=False)
        print("Wrote file: {:s}".format(out_file))

    if create_label_ycoord:
        out_file = 'label_ycoord.csv'
        d = df['YCOORD'].astype(str)
        d.to_csv(os.path.join(out_dir, out_file), index=False)
        print("Wrote file: {:s}".format(out_file))

    if create_npy:
        from sklearn.preprocessing import LabelEncoder
        from sklearn.preprocessing import OneHotEncoder

        in_dim = len(unique_chars)  # Length of a one-hot conversion
        values = np.array(list(unique_chars))
        # integer encode
        label_encoder = LabelEncoder()
        integer_encoded = label_encoder.fit_transform(values)
        # binary encode
        onehot_encoder = OneHotEncoder(sparse=False)
        integer_encoded = integer_encoded.reshape(len(integer_encoded), 1)
        onehot_encoded = onehot_encoder.fit_transform(integer_encoded)

        line_count = 0
        with open(os.path.join('data', 'parking.txt')) as f:
            for line in f:
                sample = onehot_encoder.transform(np.expand_dims(label_encoder.transform(list(line[:-1])), axis=1))
                np.savez_compressed(os.path.join('data', 'npy', 'samples', str(line_count) + '.npz'), sample)
                print('\rWriting file: {:d}.npz    '.format(line_count), end='')
                line_count += 1

        # df_labels = pd.read_csv(os.path.join('data', 'label_ycoord.csv'), header=None)
        # labels = np.empty([num_samples, label_len, len(unique_chars)])
        # for i in range(len(df_labels)):
        #     label = onehot_encoder.transform(np.expand_dims(label_encoder.transform(list(str(df_labels.iloc[i, 0]))), axis=1))
        #     labels[i] = label
        # print('Writing label NPY ...', end='')
        # np.save(os.path.join('data', 'npy', 'labels_ycoord.npy'), labels)
        # print('Done')


