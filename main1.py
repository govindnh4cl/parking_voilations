import set_random_seed
import os
import numpy as np
import pandas as pd
from keras import optimizers
from keras.models import Model
from keras.layers import Input, LSTM, Dense

num_max_samples = 132850  # Known max samples in dataset
num_samples = 32768  # Custom samples to train on
label_len = 8  # start_char, 6 digits, end_char
unique_chars = ' *,-./0123456789:>ABCDEFGHIJKLMNOPQRSTUVWXYZ_a\t\n'  # Derived by analysis

random_mapper = np.random.choice(num_samples, num_samples, replace=False)
char_map = dict([(char, idx) for idx, char in enumerate(unique_chars)])

layer_decoder = 2
layer_encoder = 2


def get_batch(batch_size, lines, labels, max_inp_len):
    batch_enc_in = np.zeros([batch_size, max_inp_len, len(unique_chars)])
    batch_dec_in = np.zeros([batch_size, label_len, len(unique_chars)])
    batch_dec_out = np.zeros(batch_dec_in.shape)

    src_idx, dst_idx = 0, 0
    while True:
        src_idx_mapped = random_mapper[src_idx]
        line = lines[src_idx_mapped]
        for idx, char in enumerate(line):
            batch_enc_in[dst_idx, idx, char_map[char]] = 1.

        label = '\t' + str(labels[src_idx_mapped]) + '\n'
        for idx, char in enumerate(label):
            batch_dec_in[dst_idx, idx, char_map[char]] = 1.
            if idx > 0:
                batch_dec_out[dst_idx, idx - 1, char_map[char]] = 1.
        src_idx += 1
        dst_idx += 1

        if src_idx >= num_samples:
            src_idx = 0

        if dst_idx >= batch_size:
            dst_idx = 0
            yield [batch_enc_in, batch_dec_in], batch_dec_out
            batch_enc_in.fill(0.)
            batch_dec_in.fill(0.)
            batch_dec_out.fill(0.)


if __name__ == '__main__':
    batch_size = 256
    nb_epoch = 300

    latent_dim = 32
    num_encoder_tokens = 48
    num_decoder_tokens = 48

    with open(os.path.join('data', 'parking.txt'), 'r') as f:
        lines = [x[:-1] for x in f]  # Remove newline character
    assert (num_samples <= num_max_samples)
    lines = lines[:num_samples]
    max_inp_len = max([len(line) for line in lines])
    print("Max inp length: {:d}".format(max_inp_len))
    labels = pd.read_csv(os.path.join('data', 'label_ycoord.csv'), header=None)[0]  # A Series datatype

    # Define an input sequence and process it.
    encoder_inputs = Input(shape=(None, num_encoder_tokens))
    if layer_encoder == 0:
        encoder = LSTM(latent_dim, return_state=True)
        encoder_outputs, state_h, state_c = encoder(encoder_inputs)
        # We discard `encoder_outputs` and only keep the states.
        encoder_states = [state_h, state_c]
    elif layer_encoder == 1:
        encoder = LSTM(latent_dim, return_sequences=True, return_state=True)(encoder_inputs)
        encoder = LSTM(latent_dim, return_state=True)(encoder)
        encoder_outputs, state_h, state_c = encoder
        # We discard `encoder_outputs` and only keep the states.
        encoder_states = [state_h, state_c]
    elif layer_encoder == 2:
        encoder_outputs0, state_h0, state_c0 = LSTM(latent_dim, return_sequences=True, return_state=True)(
            encoder_inputs)
        encoder_outputs1, state_h1, state_c1 = LSTM(latent_dim, return_sequences=True, return_state=True)(
            encoder_outputs0)
        encoder_outputs2, state_h2, state_c2 = LSTM(latent_dim, return_sequences=True, return_state=True)(
            encoder_outputs1)
        encoder_outputs3, state_h3, state_c3 = LSTM(latent_dim, return_state=True)(encoder_outputs2)
        # We discard `encoder_outputs` and only keep the states.
        encoder_states = [state_h0, state_c0, state_h1, state_c1, state_h2, state_c2, state_h3, state_c3]
    else:
        encoder0 = LSTM(latent_dim, return_sequences=True, return_state=True)(encoder_inputs)
        encoder1 = LSTM(latent_dim, return_sequences=True, return_state=True)(encoder0)
        encoder2 = LSTM(latent_dim, return_sequences=True, return_state=True)(encoder1)
        encoder3 = LSTM(latent_dim, return_sequences=True, return_state=True)(encoder2)
        encoder4 = LSTM(latent_dim, return_state=True)(encoder3)
        encoder_outputs, state_h, state_c = encoder4
        # We discard `encoder_outputs` and only keep the states.
        encoder_states = [state_h, state_c]

        # Set up the decoder, using `encoder_states` as initial state.
    decoder_inputs = Input(shape=(None, num_decoder_tokens))
    # We set up our decoder to return full output sequences,
    # and to return internal states as well. We don't use the
    # return states in the training model, but we will use them in inference.
    if layer_decoder == 0:
        decoder_lstm = LSTM(latent_dim, return_sequences=True, return_state=True)
        decoder_outputs, _, _ = decoder_lstm(decoder_inputs, initial_state=encoder_states)
    elif layer_decoder == 1:
        decoder_lstm = LSTM(latent_dim, return_sequences=True, return_state=True)
        decoder_lstm1 = LSTM(latent_dim, return_sequences=True, return_state=True)
        decoder_outputs, _, _ = decoder_lstm1(decoder_lstm(decoder_inputs, initial_state=encoder_states))
    elif layer_decoder == 2:
        decoder_lstm0, _, _ = LSTM(latent_dim, return_sequences=True, return_state=True)(decoder_inputs,
                                                                                         initial_state=encoder_states[
                                                                                                       0:2])
        decoder_lstm1, _, _ = LSTM(latent_dim, return_sequences=True, return_state=True)(decoder_lstm0,
                                                                                         initial_state=encoder_states[
                                                                                                       2:4])
        decoder_lstm2, _, _ = LSTM(latent_dim, return_sequences=True, return_state=True)(decoder_lstm1,
                                                                                         initial_state=encoder_states[
                                                                                                       4:6])
        decoder_lstm3, _, _ = LSTM(latent_dim, return_sequences=True, return_state=True)(decoder_lstm2,
                                                                                         initial_state=encoder_states[
                                                                                                       6:8])
        decoder_outputs = decoder_lstm3
    else:
        decoder_lstm0 = LSTM(latent_dim, return_sequences=True, return_state=True)
        decoder_lstm1 = LSTM(latent_dim, return_sequences=True, return_state=True)
        decoder_lstm2 = LSTM(latent_dim, return_sequences=True, return_state=True)
        decoder_lstm3 = LSTM(latent_dim, return_sequences=True, return_state=True)
        decoder_lstm4 = LSTM(latent_dim, return_sequences=True, return_state=True)
        decoder_outputs, _, _ = \
            decoder_lstm4(
                decoder_lstm3(
                    decoder_lstm2(
                        decoder_lstm1(
                            decoder_lstm0(decoder_inputs, initial_state=encoder_states)))))

    decoder_dense0 = Dense(32, activation='relu')
    decoder_dense1 = Dense(32, activation='relu')
    decoder_dense = Dense(num_decoder_tokens, activation='softmax')
    decoder_outputs = decoder_dense(decoder_dense1(decoder_dense0(decoder_outputs)))

    # Define the model that will turn
    # `encoder_inputs` & `decoder_inputs` into `decoder_outputs`
    model = Model([encoder_inputs, decoder_inputs], decoder_outputs)

    if 0:
        opt = optimizers.Adagrad(lr=0.01)
    else:
        opt = optimizers.RMSprop(lr=0.0005)

    # Run training
    model.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'])
    print(model.summary())
    model.fit_generator(get_batch(batch_size, lines, labels, max_inp_len),
                        steps_per_epoch=int(num_samples / batch_size),
                        epochs=nb_epoch)

    # Save model
    print('Saving model... ', end='')
    model.save('s2s.h5')
    print(' Done.')
    exit(0)

    # Next: inference mode (sampling).
    # Here's the drill:
    # 1) encode input and retrieve initial decoder state
    # 2) run one step of decoder with this initial state
    # and a "start of sequence" token as target.
    # Output will be the next target token
    # 3) Repeat with the current target token and current states

    # Define sampling models
    encoder_model = Model(encoder_inputs, encoder_states)

    if layer_decoder == 0:
        decoder_state_input_h = Input(shape=(latent_dim,))
        decoder_state_input_c = Input(shape=(latent_dim,))
        decoder_states_inputs = [decoder_state_input_h, decoder_state_input_c]
        decoder_outputs, state_h, state_c = decoder_lstm(
            decoder_inputs, initial_state=decoder_states_inputs)

        decoder_states = [state_h, state_c]
    elif layer_decoder == 1:
        decoder_state_input_h = Input(shape=(latent_dim,))
        decoder_state_input_c = Input(shape=(latent_dim,))
        decoder_states_inputs = [decoder_state_input_h, decoder_state_input_c]
        decoder_outputs, state_h, state_c = decoder_lstm1(
            decoder_lstm(decoder_inputs, initial_state=decoder_states_inputs))

        decoder_states = [state_h, state_c]
    elif layer_decoder == 2:
        decoder_state_input_h0 = Input(shape=(latent_dim,))
        decoder_state_input_c0 = Input(shape=(latent_dim,))
        decoder_state_input_h1 = Input(shape=(latent_dim,))
        decoder_state_input_c1 = Input(shape=(latent_dim,))
        decoder_states_inputs = [decoder_state_input_h0, decoder_state_input_c0, decoder_state_input_h1,
                                 decoder_state_input_c1]
        decoder_outputs0, state_h0, state_c0 = decoder_lstm0(decoder_inputs, initial_state=decoder_states_inputs[0:2])
        decoder_outputs1, state_h1, state_c1 = decoder_lstm1(decoder_outputs0, initial_state=decoder_states_inputs[2:4])
        decoder_outputs = decoder_outputs1

        decoder_states = [state_h0, state_c0, state_h1, state_c1]
    else:
        decoder_state_input_h = Input(shape=(latent_dim,))
        decoder_state_input_c = Input(shape=(latent_dim,))
        decoder_states_inputs = [decoder_state_input_h, decoder_state_input_c]
        decoder_outputs, state_h, state_c = \
            decoder_lstm4(
                decoder_lstm3(
                    decoder_lstm2(
                        decoder_lstm1(
                            decoder_lstm0(decoder_inputs, initial_state=decoder_states_inputs)))))
        decoder_states = [state_h, state_c]

    decoder_outputs = decoder_dense(decoder_outputs)
    decoder_model = Model(
        [decoder_inputs] + decoder_states_inputs,
        [decoder_outputs] + decoder_states)

    # Reverse-lookup token index to decode sequences back to
    # something readable.
    reverse_char_map = unique_chars


    def decode_sequence(input_seq):
        # Encode the input as state vectors.
        states_value = encoder_model.predict(input_seq)

        # Generate empty target sequence of length 1.
        target_seq = np.zeros((1, 1, num_decoder_tokens))
        # Populate the first character of target sequence with the start character.
        target_seq[0, 0, char_map['\t']] = 1.

        # Sampling loop for a batch of sequences
        # (to simplify, here we assume a batch of size 1).
        stop_condition = False
        decoded_sentence = ''
        while not stop_condition:
            output_tokens, h, c = decoder_model.predict(
                [target_seq] + states_value)

            # Sample a token
            sampled_token_index = np.argmax(output_tokens[0, -1, :])
            sampled_char = reverse_char_map[sampled_token_index]
            decoded_sentence += sampled_char

            # Exit condition: either hit max length
            # or find stop character.
            if (sampled_char == '\n' or len(decoded_sentence) > label_len):
                stop_condition = True

            # Update the target sequence (of length 1).
            target_seq = np.zeros((1, 1, num_decoder_tokens))
            target_seq[0, 0, sampled_token_index] = 1.

            # Update states
            states_value = [h, c]

        return decoded_sentence


    gen = get_batch(1, lines, labels, max_inp_len)
    for seq_index in range(30):  # Loop over batches
        # Take one sequence (part of the training set)
        # for trying out decoding.
        [input_seq, _], true_output_seq = next(gen)

        true_output_sentence = str()
        for char_code in true_output_seq[0]:  # Loop over all characters in a sample
            hot_index = np.argmax(char_code)
            output_char = reverse_char_map[hot_index]
            true_output_sentence += output_char

        pred_output_sentence = decode_sequence(input_seq)
        print('True sentence: {:s} Predicted sentence: {:s}'.format(repr(true_output_sentence),
                                                                    repr(pred_output_sentence)))
